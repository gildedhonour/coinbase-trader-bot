import numpy as np
import traceback

class StrategyManager():
    """load the strategy module"""

    def __init__(self, coinbase, strategy_name_list):
        self.strategy_object_list = []
        self.strategy_name_list = strategy_name_list
        self.coinbase = coinbase
        self.reload()

    def unload(self):
        """unload the strategy, will trigger its the __del__ method"""
        self.coinbase.signal_strategy_unload(self, None)
        self.strategy_object_list = []

    def reload(self):
        """reload and re-initialize the strategy module"""
        self.unload()
        for name_raw in self.strategy_name_list:
            name = name_raw.replace(".py", "").strip()
            try:
                strategy_module = __import__(name)
                try:
                    reload(strategy_module)
                    strategy_object = strategy_module.Strategy(self.coinbase)
                    self.strategy_object_list.append(strategy_object)
                    if hasattr(strategy_object, "name"):
                        self.coinbase.strategies[strategy_object.name] = strategy_object

                except Exception:
                    self.coinbase.debug("### error while loading strategy %s.py, traceback follows:" % name)
                    self.coinbase.debug(traceback.format_exc())

            except ImportError:
                self.coinbase.debug("### could not import %s.py, traceback follows:" % name)
                self.coinbase.debug(traceback.format_exc())


class Strategy(object):
  def update(self, data):
      pass

  def seen_enough_data(self):
      return False

  def amount_of_data_still_missing(self):
      return 0


class Candle(object):
  def __init__(self):
    self.open = 0.0
    self.high = 0.0
    self.low = 0.0
    self.close = 0.0
    self.start_time = 0.0
    self.end_time = 0.0


class MACrossOver(Strategy):
  slow_ma_window = 52
  fast_ma_window = 7

  def __init__(self, client):
    self._slow_ma = []
    self._fast_ma = []
    self.client = client

  def seen_enough_data(self):
    if len(self._slow_ma) < self.slow_ma_window:
      return False

  def amount_of_data_still_missing(self):
    return max(abs(self.slow_ma_window - len(self._slow_ma)), 0)

  def update(self, data):
    if not isinstance(data, Candle):
      return

    if not self.seen_enough_data():
      return

    slow_ma_value = np.mean([self._slow_ma[ix].close for ix in range(0, len(self._slow_ma))])
    fast_ma_value = np.mean([self._fast_ma[ix].close for ix in range(0, len(self._fast_ma))])

    if (slow_ma_value > fast_ma_value):
      self.client.sell(amount)


    if (fast_ma_value > slow_ma_value):
      self.client.buy(amount)