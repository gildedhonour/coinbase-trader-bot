import json
import strategy
import argparse
import curses
import curses.panel
import curses.textpad
import coinbase_api
import logging
import locale
import math
import os
import sys
import time
import traceback
import threading
import gui
import strategy

class LogWriter():
    """connects to coinbase.signal_debug and logs it all to the logfile"""
    def __init__(self, coinbase):
        self.coinbase = coinbase
        logging.basicConfig(filename='coinbase.log'
                           ,filemode="w"
                           ,format='%(asctime)s:%(levelname)s:%(message)s'
                           ,level=logging.DEBUG
                           )
        self.coinbase.signal_debug.connect(self.slot_debug)

    def close(self):
        """stop logging"""
        #not needed
        pass

    # pylint: disable=R0201
    def slot_debug(self, sender, (msg)):
        """handler for signal_debug signals"""
        name = "%s.%s" % (sender.__class__.__module__, sender.__class__.__name__)
        logging.debug("%s:%s", name, msg)


class PrintHook():
    """intercept stdout/stderr and send it all to coinbase.signal_debug instead"""
    def __init__(self, coinbase):
        self.coinbase = coinbase
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        sys.stdout = self
        sys.stderr = self

    def close(self):
        """restore normal stdio"""
        sys.stdout = self.stdout
        sys.stderr = self.stderr

    def write(self, string):
        """called when someone uses print(), send it to coinbase"""
        string = string.strip()
        if string != "":
            self.coinbase.signal_debug(self, string)


def try_get_lock_or_break_open():
    """this is an ugly hack to workaround possible deadlock problems.
    It is used during shutdown to make sure we can properly exit even when
    some slot is stuck (due to a programming error) and won't release the lock.
    If we can't acquire it within 2 seconds we just break it open forcefully."""
    #pylint: disable=W0212
    time_end = time.time() + 2
    while time.time() < time_end:
        if coinbase_api.Signal._lock.acquire(False):
            return
        time.sleep(0.001)

    # something keeps holding the lock, apparently some slot is stuck
    # in an infinite loop. In order to be able to shut down anyways
    # we just throw away that lock and replace it with a new one
    lock = threading.RLock()
    lock.acquire()
    coinbase_api.Signal._lock = lock
    print "### could not acquire signal lock, frozen slot somewhere?"
    print "### please see the stacktrace log to determine the cause."

def dump_all_stacks():
  """dump a stack trace for all running threads for debugging purpose"""

  def get_name(thread_id):
    """return the human readable name that was assigned to a thread"""
    for thread in threading.enumerate():
      if thread.ident == thread_id:
        return thread.name

  ret = "\n# Full stack trace of all running threads:\n"
  #pylint: disable=W0212
  for thread_id, stack in sys._current_frames().items():
    ret += "\n# %s (%s)\n" % (get_name(thread_id), thread_id)
    for filename, lineno, name, line in traceback.extract_stack(stack):
      ret += 'File: "%s", line %d, in %s\n' % (filename, lineno, name)
      if line:
        ret += "  %s\n" % (line.strip())
  
  return ret

def main():
  debug_tb = []
  def curses_loop(stdscr):
    # try:
    init_colors()
    
    # import pdb; pdb.set_trace()
    
    coinbase = coinbase_api.Coinbase()
    logwriter = LogWriter(coinbase)
    printhook = PrintHook(coinbase)
    
    conwin = gui.WinConsole(stdscr, coinbase)
    bookwin = gui.WinOrderBook(stdscr, coinbase)
    status_win = gui.WinStatus(stdscr, coinbase)
    chartwin = gui.WinChart(stdscr, coinbase)
    
    # strategy_manager = strategy.StrategyManager(coinbase, strat_mod_list)
    strategy_manager = strategy.StrategyManager(coinbase, ["strategy.py"])
    coinbase.start()
    while True:
      key = stdscr.getch()
      if key == ord("q"):
        break
      elif key == curses.KEY_F4:
        DlgNewOrderBid(stdscr, coinbase).modal()
      elif key == curses.KEY_F5:
        DlgNewOrderAsk(stdscr, coinbase).modal()
      elif key == curses.KEY_F6:
        DlgCancelOrders(stdscr, coinbase).modal()
      elif key == curses.KEY_RESIZE:
        with coinbase_api.Signal._lock:
          stdscr.erase()
          stdscr.refresh()
          conwin.resize()
          bookwin.resize()
          chartwin.resize()
          status_win.resize()
      elif key == ord("l"):
        strategy_manager.reload()

      # which chart to show on the right side
      elif key == ord("H"):
        set_ini(coinbase, "display_right", "history_chart", coinbase.history.signal_changed, coinbase.history, None)
      elif key == ord("D"):
        set_ini(coinbase, "display_right", "depth_chart", coinbase.orderbook.signal_changed, coinbase.orderbook, None)

      #  depth chart step
      elif key == ord(","): # zoom out
        toggle_depth_group(coinbase, +1)
      elif key == ord("."): # zoom in
        toggle_depth_group(coinbase, -1)

      # orderbook grouping step
      elif key == ord("-"): # zoom out (larger step)
        toggle_orderbook_group(coinbase, +1)
      elif key == ord("+"): # zoom in (smaller step)
        toggle_orderbook_group(coinbase, -1)

      elif key == ord("S"):
        toggle_orderbook_sum(coinbase)

      elif key == ord("T"):
        toggle_depth_sum(coinbase)

      # lowercase keys go to the strategy module
      elif key >= ord("a") and key <= ord("z"):
        coinbase.signal_keypress(coinbase, (key))
      else:
        coinbase.debug("key pressed: key=%i" % key)

    # except KeyboardInterrupt:
    #   pass

    # except Exception as e:
    #   print("Exception: {}".format(e))
      # we are here because shutdown was requested.
      #
      # Before we do anything we dump stacktraces of all currently running
      # threads to a separate logfile because this helps debugging freezes
      # and deadlocks that might occur if things went totally wrong.
    
  with open("coinbase_stacktrace.log", "w") as stacklog:
    stacklog.write(dump_all_stacks())

    # we need the signal lock to be able to shut down. And we cannot
    # wait for any frozen slot to return, so try really hard to get
    # the lock and if that fails then unlock it forcefully.
  try_get_lock_or_break_open()

    # Now trying to shutdown everything in an orderly manner.it in the
    # Since we are still inside curses but we don't know whether
    # the printhook or the logwriter was initialized properly already
    # or whether it crashed earlier we cannot print here and we also
    # cannot log, so we put all tracebacks into the debug_tb list to
    # print them later once the terminal is properly restored again.
  try:
    strategy_manager.unload()
  except Exception:
    debug_tb.append(traceback.format_exc())

  try:
    coinbase.stop()
  except Exception:
    debug_tb.append(traceback.format_exc())

  try:
    # printhook.close()
    pass
  except Exception:
    debug_tb.append(traceback.format_exc())

  try:
    # logwriter.close()
    pass
  except Exception:
    debug_tb.append(traceback.format_exc())

  curses.wrapper(curses_loop)

def init_colors():
  index = 1
  for (name, back, fore) in COLORS:
    if curses.has_colors():
      curses.init_pair(index, fore, back)
      COLOR_PAIR[name] = curses.color_pair(index)
    else:
      COLOR_PAIR[name] = 0
    
    index += 1

COLOR_PAIR = {}
COLORS =    [["con_text",       curses.COLOR_BLUE,    curses.COLOR_CYAN]
            ,["con_text_buy",   curses.COLOR_BLUE,    curses.COLOR_GREEN]
            ,["con_text_sell",  curses.COLOR_BLUE,    curses.COLOR_RED]
            ,["status_text",    curses.COLOR_BLUE,    curses.COLOR_CYAN]

            ,["book_text",      curses.COLOR_BLACK,   curses.COLOR_CYAN]
            ,["book_bid",       curses.COLOR_BLACK,   curses.COLOR_GREEN]
            ,["book_ask",       curses.COLOR_BLACK,   curses.COLOR_RED]
            ,["book_own",       curses.COLOR_BLACK,   curses.COLOR_YELLOW]
            ,["book_vol",       curses.COLOR_BLACK,   curses.COLOR_CYAN]

            ,["chart_text",     curses.COLOR_BLACK,   curses.COLOR_WHITE]
            ,["chart_up",       curses.COLOR_BLACK,   curses.COLOR_GREEN]
            ,["chart_down",     curses.COLOR_BLACK,   curses.COLOR_RED]
            ,["order_pending",  curses.COLOR_BLACK,   curses.COLOR_RED]

            ,["dialog_text",     curses.COLOR_BLUE,   curses.COLOR_CYAN]
            ,["dialog_sel",      curses.COLOR_CYAN,   curses.COLOR_BLUE]
            ,["dialog_sel_text", curses.COLOR_BLUE,   curses.COLOR_YELLOW]
            ,["dialog_sel_sel",  curses.COLOR_YELLOW, curses.COLOR_BLUE]
            ,["dialog_bid_text", curses.COLOR_GREEN,  curses.COLOR_BLACK]
            ,["dialog_ask_text", curses.COLOR_RED,    curses.COLOR_WHITE]
            ]


if __name__ == "__main__":
  main()

  # import pdb; pdb.set_trace()