import argparse
import curses
import curses.panel
import curses.textpad
import coinbase_api
import logging
import locale
import math
import os
import sys
import time
import threading

HEIGHT_STATUS   = 2
HEIGHT_CON      = 7
WIDTH_ORDERBOOK = 45

class Win:
    def __init__(self, stdscr):
        self.stdscr = stdscr
        self.posx = 0
        self.posy = 0
        self.width = 10
        self.height = 10
        self.termwidth = 10
        self.termheight = 10
        self.win = None
        self.panel = None
        self.__create_win()

    def __del__(self):
        del self.panel
        del self.win
        curses.panel.update_panels()
        curses.doupdate()

    def calc_size(self):
        """override this method to change posx, posy, width, height.
        It will be called before window creation and on resize."""
        pass

    def do_paint(self):
        """call this if you want the window to repaint itself"""
        curses.curs_set(0)
        if self.win:
            self.paint()
            self.done_paint()

    # method could be a function - pylint: disable=R0201
    def done_paint(self):
        """update the sreen after paint operations, this will invoke all
        necessary stuff to refresh all (possibly overlapping) windows in
        the right order and then push it to the screen"""
        curses.panel.update_panels()
        curses.doupdate()

    def paint(self):
        """paint the window. Override this with your own implementation.
        This method must paint the entire window contents from scratch.
        It is automatically called after the window has been initially
        created and also after every resize. Call it explicitly when
        your data has changed and must be displayed"""
        pass

    def resize(self):
        """You must call this method from your main loop when the
        terminal has been resized. It will subsequently make it
        recalculate its own new size and then call its paint() method"""
        del self.win
        self.__create_win()

    def addstr(self, *args):
        """drop-in replacement for addstr that will never raie exceptions
        and that will cut off at end of line instead of wrapping"""
        if len(args) > 0:
            line, col = self.win.getyx()
            string = args[0]
            attr = 0
        if len(args) > 1:
            attr = args[1]
        if len(args) > 2:
            line, col, string = args[:3]
            attr = 0
        if len(args) > 3:
            attr = args[3]
        if line >= self.height:
            return
        space_left = self.width - col - 1 #always omit last column, avoids problems.
        if space_left <= 0:
            return
        self.win.addstr(line, col, string[:space_left], attr)

    def addch(self, posy, posx, character, color_pair):
        """place a character but don't throw error in lower right corner"""
        if posy < 0 or posy > self.height - 1:
            return
        if posx < 0 or posx > self.width - 1:
            return
        if posx == self.width - 1 and posy == self.height - 1:
            return
        self.win.addch(posy, posx, character, color_pair)

    def __create_win(self):
        """create the window. This will also be called on every resize,
        windows won't be moved, they will be deleted and recreated."""
        self.__calc_size()
        try:
            self.win = curses.newwin(self.height, self.width, self.posy, self.posx)
            self.panel = curses.panel.new_panel(self.win)
            self.win.scrollok(True)
            self.win.keypad(1)
            self.do_paint()
        except Exception:
            self.win = None
            self.panel = None

    def __calc_size(self):
        """calculate the default values for positionand size. By default
        this will result in a window covering the entire terminal.
        Implement the calc_size() method (which will be called afterwards)
        to change (some of) these values according to your needs."""
        maxyx = self.stdscr.getmaxyx()
        self.termwidth = maxyx[1]
        self.termheight = maxyx[0]
        self.posx = 0
        self.posy = 0
        self.width = self.termwidth
        self.height = self.termheight
        self.calc_size()


class WinStatus(Win):
    """the status window at the top"""

    def __init__(self, stdscr, gox):
        """create the status window and connect the needed callbacks"""
        self.gox = gox
        self.order_lag = 0
        self.order_lag_txt = ""
        self.sorted_currency_list = []
        gox.signal_orderlag.connect(self.slot_orderlag)
        gox.signal_wallet.connect(self.slot_changed)
        gox.orderbook.signal_changed.connect(self.slot_changed)
        Win.__init__(self, stdscr)

    def calc_size(self):
        """place it at the top of the terminal"""
        self.height = HEIGHT_STATUS

    def sort_currency_list_if_changed(self):
        """sort the currency list in the wallet for better display,
        sort it only if it has changed, otherwise leave it as it is"""
        currency_list = self.gox.wallet.keys()
        if len(currency_list) == len(self.sorted_currency_list):
            return

        # now we will bring base and quote currency to the front and sort the
        # the rest of the list of names by acount balance in descending order
        if self.gox.curr_base in currency_list:
            currency_list.remove(self.gox.curr_base)
        if self.gox.curr_quote in currency_list:
            currency_list.remove(self.gox.curr_quote)
        currency_list.sort(key=lambda name: -self.gox.wallet[name])
        currency_list.insert(0, self.gox.curr_quote)
        currency_list.insert(0, self.gox.curr_base)
        self.sorted_currency_list = currency_list

    def paint(self):
        """paint the complete status"""
        cbase = self.gox.curr_base
        cquote = self.gox.curr_quote
        self.sort_currency_list_if_changed()
        self.win.bkgd(" ", COLOR_PAIR["status_text"])
        self.win.erase()

        #
        # first line
        #
        line1 = "Market: %s%s | " % (cbase, cquote)
        line1 += "Account: "
        if len(self.sorted_currency_list):
            for currency in self.sorted_currency_list:
                if currency in self.gox.wallet:
                    line1 += currency + " " \
                    + goxapi.int2str(self.gox.wallet[currency], currency).strip() \
                    + " + "
            line1 = line1.strip(" +")
        else:
            line1 += "No info (yet)"

        #
        # second line
        #
        line2 = ""
        if self.gox.config.get_bool("goxtool", "show_orderbook_stats"):
            str_btc = locale.format('%d', self.gox.orderbook.total_ask, 1)
            str_fiat = locale.format('%d', self.gox.orderbook.total_bid, 1)
            if self.gox.orderbook.total_ask:
                str_ratio = locale.format('%1.2f',
                    self.gox.orderbook.total_bid / self.gox.orderbook.total_ask, 1)
            else:
                str_ratio = "-"

            line2 += "sum_bid: %s %s | " % (str_fiat, cquote)
            line2 += "sum_ask: %s %s | " % (str_btc, cbase)
            line2 += "ratio: %s %s/%s | " % (str_ratio, cquote, cbase)

        line2 += "o_lag: %s | " % self.order_lag_txt
        line2 += "s_lag: %.3f s" % (self.gox.socket_lag / 1e6)
        self.addstr(0, 0, line1, COLOR_PAIR["status_text"])
        self.addstr(1, 0, line2, COLOR_PAIR["status_text"])


    def slot_changed(self, dummy_sender, dummy_data):
        """the callback funtion called by the Gox() instance"""
        self.do_paint()

    def slot_orderlag(self, dummy_sender, (usec, text)):
        """slot for order_lag mesages"""
        self.order_lag = usec
        self.order_lag_txt = text
        self.do_paint()


class WinConsole(Win):
    """The console window at the bottom"""
    def __init__(self, stdscr, gox):
        """create the console window and connect it to the Gox debug
        callback function"""
        self.gox = gox
        gox.signal_debug.connect(self.slot_debug)
        Win.__init__(self, stdscr)

    def paint(self):
        """just empty the window after resize (I am lazy)"""
        self.win.bkgd(" ", COLOR_PAIR["con_text"])

    def resize(self):
        """resize and print a log message. Old messages will have been
        lost after resize because of my dumb paint() implementation, so
        at least print a message indicating that fact into the
        otherwise now empty console window"""
        Win.resize(self)
        self.write("### console has been resized")

    def calc_size(self):
        """put it at the bottom of the screen"""
        self.height = HEIGHT_CON
        self.posy = self.termheight - self.height

    def slot_debug(self, dummy_gox, (txt)):
        """this slot will be connected to all debug signals."""
        self.write(txt)

    def write(self, txt):
        """write a line of text, scroll if needed"""
        if not self.win:
            return

        # This code would break if the format of
        # the log messages would ever change!
        if " tick:" in txt:
            if not self.gox.config.get_bool("goxtool", "show_ticker"):
                return
        if "depth:" in txt:
            if not self.gox.config.get_bool("goxtool", "show_depth"):
                return
        if "trade:" in txt:
            if "own order" in txt:
                if not self.gox.config.get_bool("goxtool", "show_trade_own"):
                    return
            else:
                if not self.gox.config.get_bool("goxtool", "show_trade"):
                    return

        col = COLOR_PAIR["con_text"]
        if "trade: bid:" in txt:
            col = COLOR_PAIR["con_text_buy"] + curses.A_BOLD
        if "trade: ask:" in txt:
            col = COLOR_PAIR["con_text_sell"] + curses.A_BOLD
        self.win.addstr("\n" + txt,  col)
        self.done_paint()

class WinOrderBook(Win):
    """the orderbook window"""

    def __init__(self, stdscr, gox):
        """create the orderbook window and connect it to the
        onChanged callback of the gox.orderbook instance"""
        self.gox = gox
        gox.orderbook.signal_changed.connect(self.slot_changed)
        Win.__init__(self, stdscr)

    def calc_size(self):
        """put it into the middle left side"""
        self.height = self.termheight - HEIGHT_CON - HEIGHT_STATUS
        self.posy = HEIGHT_STATUS
        self.width = WIDTH_ORDERBOOK

    def paint(self):
        """paint the visible portion of the orderbook"""

        def paint_row(pos, price, vol, ownvol, color, changevol):
            """paint a row in the orderbook (bid or ask)"""
            if changevol > 0:
                col2 = col_bid + curses.A_BOLD
            elif changevol < 0:
                col2 = col_ask + curses.A_BOLD
            else:
                col2 = col_vol
            self.addstr(pos, 0,  book.gox.quote2str(price), color)
            self.addstr(pos, 12, book.gox.base2str(vol), col2)
            if ownvol:
                self.addstr(pos, 28, book.gox.base2str(ownvol), col_own)

        self.win.bkgd(" ",  COLOR_PAIR["book_text"])
        self.win.erase()

        gox = self.gox
        book = gox.orderbook

        mid = self.height / 2
        col_bid = COLOR_PAIR["book_bid"]
        col_ask = COLOR_PAIR["book_ask"]
        col_vol = COLOR_PAIR["book_vol"]
        col_own = COLOR_PAIR["book_own"]

        sum_total = gox.config.get_bool("goxtool", "orderbook_sum_total")
        group = gox.config.get_float("goxtool", "orderbook_group")
        group = gox.quote2int(group)
        if group == 0:
            group = 1

        #
        #
        # paint the asks (first we put them into bins[] then we paint them)
        #
        if len(book.asks):
            i = 0
            bins = []
            pos = mid - 1
            vol = 0
            prev_vol = 0

            # no grouping, bins can be created in one simple and fast loop
            if group == 1:
                cnt = len(book.asks)
                while pos >= 0 and i < cnt:
                    level = book.asks[i]
                    price = level.price
                    if sum_total:
                        vol += level.volume
                    else:
                        vol = level.volume
                    ownvol = level.own_volume
                    bins.append([pos, price, vol, ownvol, 0])
                    pos -= 1
                    i += 1

            # with gouping its a bit more complicated
            else:
                # first bin is exact lowest ask price
                price = book.asks[0].price
                vol = book.asks[0].volume
                bins.append([pos, price, vol, 0, 0])
                prev_vol = vol
                pos -= 1

                # now all following bins
                bin_price = int(math.ceil(float(price) / group) * group)
                if bin_price == price:
                    # first level was exact bin price already, skip to next bin
                    bin_price += group
                while pos >= 0 and bin_price < book.asks[-1].price + group:
                    vol, _vol_quote = book.get_total_up_to(bin_price, True)          ## 01 freeze
                    if vol > prev_vol:
                        # append only non-empty bins
                        if sum_total:
                            bins.append([pos, bin_price, vol, 0, 0])
                        else:
                            bins.append([pos, bin_price, vol - prev_vol, 0, 0])
                        prev_vol = vol
                        pos -= 1
                    bin_price += group

                # now add the own volumes to their bins
                for order in book.owns:
                    if order.typ == "ask" and order.price > 0:
                        order_bin_price = int(math.ceil(float(order.price) / group) * group)
                        for abin in bins:
                            if abin[1] == order.price:
                                abin[3] += order.volume
                                break
                            if abin[1] == order_bin_price:
                                abin[3] += order.volume
                                break

            # mark the level where change took place (optional)
            if gox.config.get_bool("goxtool", "highlight_changes"):
                if book.last_change_type == "ask":
                    change_bin_price = int(math.ceil(float(book.last_change_price) / group) * group)
                    for abin in bins:
                        if abin[1] == book.last_change_price:
                            abin[4] = book.last_change_volume
                            break
                        if abin[1] == change_bin_price:
                            abin[4] = book.last_change_volume
                            break

            # now finally paint the asks
            for pos, price, vol, ownvol, changevol in bins:
                paint_row(pos, price, vol, ownvol, col_ask, changevol)

        #
        #
        # paint the bids (first we put them into bins[] then we paint them)
        #
        if len(book.bids):
            i = 0
            bins = []
            pos = mid + 1
            vol = 0
            prev_vol = 0

            # no grouping, bins can be created in one simple and fast loop
            if group == 1:
                cnt = len(book.bids)
                while pos < self.height and i < cnt:
                    level = book.bids[i]
                    price = level.price
                    if sum_total:
                        vol += level.volume
                    else:
                        vol = level.volume
                    ownvol = level.own_volume
                    bins.append([pos, price, vol, ownvol, 0])
                    prev_vol = vol
                    pos += 1
                    i += 1

            # with gouping its a bit more complicated
            else:
                # first bin is exact lowest ask price
                price = book.bids[0].price
                vol = book.bids[0].volume
                bins.append([pos, price, vol, 0, 0])
                prev_vol = vol
                pos += 1

                # now all following bins
                bin_price = int(math.floor(float(price) / group) * group)
                if bin_price == price:
                    # first level was exact bin price already, skip to next bin
                    bin_price -= group
                while pos < self.height and bin_price >= 0:
                    vol, _vol_quote = book.get_total_up_to(bin_price, False)
                    if vol > prev_vol:
                        # append only non-empty bins
                        if sum_total:
                            bins.append([pos, bin_price, vol, 0, 0])
                        else:
                            bins.append([pos, bin_price, vol - prev_vol, 0, 0])
                        prev_vol = vol
                        pos += 1
                    bin_price -= group

                # now add the own volumes to their bins
                for order in book.owns:
                    if order.typ == "bid" and order.price > 0:
                        order_bin_price = int(math.floor(float(order.price) / group) * group)
                        for abin in bins:
                            if abin[1] == order.price:
                                abin[3] += order.volume
                                break
                            if abin[1] == order_bin_price:
                                abin[3] += order.volume
                                break

            # mark the level where change took place (optional)
            if gox.config.get_bool("goxtool", "highlight_changes"):
                if book.last_change_type == "bid":
                    change_bin_price = int(math.floor(float(book.last_change_price) / group) * group)
                    for abin in bins:
                        if abin[1] == book.last_change_price:
                            abin[4] = book.last_change_volume
                            break
                        if abin[1] == change_bin_price:
                            abin[4] = book.last_change_volume
                            break

            # now finally paint the bids
            for pos, price, vol, ownvol, changevol in bins:
                paint_row(pos, price, vol, ownvol, col_bid, changevol)

        # update the xterm title bar
        if self.gox.config.get_bool("goxtool", "set_xterm_title"):
            last_candle = self.gox.history.last_candle()
            if last_candle:
                title = self.gox.quote2str(last_candle.cls).strip()
                title += " - goxtool -"
                title += " bid:" + self.gox.quote2str(book.bid).strip()
                title += " ask:" + self.gox.quote2str(book.ask).strip()

                term = os.environ["TERM"]
                # the following is incomplete but better safe than sorry
                # if you know more terminals then please provide a patch
                if "xterm" in term or "rxvt" in term:
                    sys_out.write("\x1b]0;%s\x07" % title)
                    sys_out.flush()

    def slot_changed(self, _book, _dummy):
        """Slot for orderbook.signal_changed"""
        self.do_paint()


TYPE_HISTORY = 1
TYPE_ORDERBOOK = 2

class WinChart(Win):
    """the chart window"""

    def __init__(self, stdscr, gox):
        self.gox = gox
        self.pmin = 0
        self.pmax = 0
        self.change_type = None
        gox.history.signal_changed.connect(self.slot_history_changed)
        gox.orderbook.signal_changed.connect(self.slot_orderbook_changed)

        # some terminals do not support reverse video
        # so we cannot use reverse space for candle bodies
        if curses.A_REVERSE & curses.termattrs():
            self.body_char = " "
            self.body_attr = curses.A_REVERSE
        else:
            self.body_char = curses.ACS_CKBOARD # pylint: disable=E1101
            self.body_attr = 0

        Win.__init__(self, stdscr)

    def calc_size(self):
        """position in the middle, right to the orderbook"""
        self.posx = WIDTH_ORDERBOOK
        self.posy = HEIGHT_STATUS
        self.width = self.termwidth - WIDTH_ORDERBOOK
        self.height = self.termheight - HEIGHT_CON - HEIGHT_STATUS

    def is_in_range(self, price):
        """is this price in the currently visible range?"""
        return price <= self.pmax and price >= self.pmin

    def get_optimal_step(self, num_min):
        """return optimal step size for painting y-axis labels so that the
        range will be divided into at least num_min steps"""
        if self.pmax <= self.pmin:
            return None
        stepex = float(self.pmax - self.pmin) / num_min
        step1 = math.pow(10, math.floor(math.log(stepex, 10)))
        step2 = step1 * 2
        step5 = step1 * 5
        if step5 <= stepex:
            return step5
        if step2 <= stepex:
            return step2
        return step1

    def price_to_screen(self, price):
        """convert price into screen coordinates (y=0 is at the top!)"""
        relative_from_bottom = \
            float(price - self.pmin) / float(self.pmax - self.pmin)
        screen_from_bottom = relative_from_bottom * self.height
        return int(self.height - screen_from_bottom)

    def paint_y_label(self, posy, posx, price):
        """paint the y label of the history chart, formats the number
        so that it needs not more room than necessary but it also uses
        pmax to determine how many digits are needed so that all numbers
        will be nicely aligned at the decimal point"""

        fprice = self.gox.quote2float(price)
        labelstr = ("%f" % fprice).rstrip("0").rstrip(".")

        # look at pmax to determine the max number of digits before the decimal
        # and then pad all smaller prices with spaces to make them align nicely.
        need_digits = int(math.log10(self.gox.quote2float(self.pmax))) + 1
        have_digits = len(str(int(fprice)))
        if have_digits < need_digits:
            padding = " " * (need_digits - have_digits)
            labelstr = padding + labelstr

        self.addstr(
            posy, posx,
            labelstr,
            COLOR_PAIR["chart_text"]
        )

    def paint_candle(self, posx, candle):
        """paint a single candle"""

        sopen  = self.price_to_screen(candle.opn)
        shigh  = self.price_to_screen(candle.hig)
        slow   = self.price_to_screen(candle.low)
        sclose = self.price_to_screen(candle.cls)

        for posy in range(self.height):
            if posy >= shigh and posy < sopen and posy < sclose:
                # upper wick
                # pylint: disable=E1101
                self.addch(posy, posx, curses.ACS_VLINE, COLOR_PAIR["chart_text"])
            if posy >= sopen and posy < sclose:
                # red body
                self.addch(posy, posx, self.body_char, self.body_attr + COLOR_PAIR["chart_down"])
            if posy >= sclose and posy < sopen:
                # green body
                self.addch(posy, posx, self.body_char, self.body_attr + COLOR_PAIR["chart_up"])
            if posy >= sopen and posy >= sclose and posy < slow:
                # lower wick
                # pylint: disable=E1101
                self.addch(posy, posx, curses.ACS_VLINE, COLOR_PAIR["chart_text"])

    def paint(self):
        typ = self.gox.config.get_string("goxtool", "display_right")
        if typ == "history_chart":
            self.paint_history_chart()
        elif typ == "depth_chart":
            self.paint_depth_chart()
        else:
            self.paint_history_chart()

    def paint_depth_chart(self):
        """paint a depth chart"""

        # pylint: disable=C0103
        if self.gox.curr_quote in "JPY SEK":
            BAR_LEFT_EDGE = 7
            FORMAT_STRING = "%6.0f"
        else:
            BAR_LEFT_EDGE = 8
            FORMAT_STRING = "%7.2f"

        def paint_depth(pos, price, vol, own, col_price, change):
            """paint one row of the depth chart"""
            if change > 0:
                col = col_bid + curses.A_BOLD
            elif change < 0:
                col = col_ask + curses.A_BOLD
            else:
                col = col_bar
            pricestr = FORMAT_STRING % self.gox.quote2float(price)
            self.addstr(pos, 0, pricestr, col_price)
            length = int(vol * mult_x)
            # pylint: disable=E1101
            self.win.hline(pos, BAR_LEFT_EDGE, curses.ACS_CKBOARD, length, col)
            if own:
                self.addstr(pos, length + BAR_LEFT_EDGE, "o", col_own)

        self.win.bkgd(" ",  COLOR_PAIR["chart_text"])
        self.win.erase()

        book = self.gox.orderbook
        if not (book.bid and book.ask and len(book.bids) and len(book.asks)):
            # orderbook is not initialized yet, paint nothing
            return

        col_bar = COLOR_PAIR["book_vol"]
        col_bid = COLOR_PAIR["book_bid"]
        col_ask = COLOR_PAIR["book_ask"]
        col_own = COLOR_PAIR["book_own"]

        group = self.gox.config.get_float("goxtool", "depth_chart_group")
        if group == 0:
            group = 1
        group = self.gox.quote2int(group)

        max_vol_ask = 0
        max_vol_bid = 0
        bin_asks = []
        bin_bids = []
        mid = self.height / 2
        sum_total = self.gox.config.get_bool("goxtool", "depth_chart_sum_total")

        #
        #
        # bin the asks
        #
        pos = mid - 1
        prev_vol = 0
        bin_price = int(math.ceil(float(book.asks[0].price) / group) * group)
        while pos >= 0 and bin_price < book.asks[-1].price + group:
            bin_vol, _bin_vol_quote = book.get_total_up_to(bin_price, True)
            if bin_vol > prev_vol:
                # add only non-empty bins
                if sum_total:
                    bin_asks.append([pos, bin_price, bin_vol, 0, 0])
                    max_vol_ask = max(bin_vol, max_vol_ask)
                else:
                    bin_asks.append([pos, bin_price, bin_vol - prev_vol, 0, 0])
                    max_vol_ask = max(bin_vol - prev_vol, max_vol_ask)
                prev_vol = bin_vol
                pos -= 1
            bin_price += group

        #
        #
        # bin the bids
        #
        pos = mid + 1
        prev_vol = 0
        bin_price = int(math.floor(float(book.bids[0].price) / group) * group)
        while pos < self.height and bin_price >= 0:
            _bin_vol_base, bin_vol_quote = book.get_total_up_to(bin_price, False)
            bin_vol = self.gox.base2int(bin_vol_quote / book.bid)
            if bin_vol > prev_vol:
                # add only non-empty bins
                if sum_total:
                    bin_bids.append([pos, bin_price, bin_vol, 0, 0])
                    max_vol_bid = max(bin_vol, max_vol_bid)
                else:
                    bin_bids.append([pos, bin_price, bin_vol - prev_vol, 0, 0])
                    max_vol_bid = max(bin_vol - prev_vol, max_vol_bid)
                prev_vol = bin_vol
                pos += 1
            bin_price -= group

        max_vol_tot = max(max_vol_ask, max_vol_bid)
        if not max_vol_tot:
            return
        mult_x = float(self.width - BAR_LEFT_EDGE - 2) / max_vol_tot

        # add the own volume to the bins
        for order in book.owns:
            if order.price > 0:
                if order.typ == "ask":
                    bin_price = int(math.ceil(float(order.price) / group) * group)
                    for abin in bin_asks:
                        if abin[1] == bin_price:
                            abin[3] += order.volume
                            break
                else:
                    bin_price = int(math.floor(float(order.price) / group) * group)
                    for abin in bin_bids:
                        if abin[1] == bin_price:
                            abin[3] += order.volume
                            break

        # highlight the relative change (optional)
        if self.gox.config.get_bool("goxtool", "highlight_changes"):
            price = book.last_change_price
            if book.last_change_type == "ask":
                bin_price = int(math.ceil(float(price) / group) * group)
                for abin in bin_asks:
                    if abin[1] == bin_price:
                        abin[4] = book.last_change_volume
                        break
            if book.last_change_type == "bid":
                bin_price = int(math.floor(float(price) / group) * group)
                for abin in bin_bids:
                    if abin[1] == bin_price:
                        abin[4] = book.last_change_volume
                        break

        # paint the asks
        for pos, price, vol, own, change in bin_asks:
            paint_depth(pos, price, vol, own, col_ask, change)

        # paint the bids
        for pos, price, vol, own, change in bin_bids:
            paint_depth(pos, price, vol, own, col_bid, change)

    def paint_history_chart(self):
        """paint a history candlestick chart"""

        if self.change_type == TYPE_ORDERBOOK:
            # erase only the rightmost column to redraw bid/ask and orders
            # beause we won't redraw the chart, its only an orderbook change
            self.win.vline(0, self.width - 1, " ", self.height, COLOR_PAIR["chart_text"])
        else:
            self.win.bkgd(" ",  COLOR_PAIR["chart_text"])
            self.win.erase()

        hist = self.gox.history
        book = self.gox.orderbook

        self.pmax = 0
        self.pmin = 9999999999

        # determine y range
        posx = self.width - 2
        index = 0
        while index < hist.length() and posx >= 0:
            candle = hist.candles[index]
            if self.pmax < candle.hig:
                self.pmax = candle.hig
            if self.pmin > candle.low:
                self.pmin = candle.low
            index += 1
            posx -= 1

        if self.pmax == self.pmin:
            return

        # paint the candlestick chart.
        # We won't paint it if it was triggered from an orderbook change
        # signal because that would be redundant and only waste CPU.
        # In that case we only repaint the bid/ask markers (see below)
        if self.change_type != TYPE_ORDERBOOK:
            # paint the candles
            posx = self.width - 2
            index = 0
            while index < hist.length() and posx >= 0:
                candle = hist.candles[index]
                self.paint_candle(posx, candle)
                index += 1
                posx -= 1

            # paint the y-axis labels
            posx = 0
            step = self.get_optimal_step(4)
            if step:
                labelprice = int(self.pmin / step) * step
                while not labelprice > self.pmax:
                    posy = self.price_to_screen(labelprice)
                    if posy < self.height - 1:
                        self.paint_y_label(posy, posx, labelprice)
                    labelprice += step

        # paint bid, ask, own orders
        posx = self.width - 1
        for order in book.owns:
            if self.is_in_range(order.price):
                posy = self.price_to_screen(order.price)
                if order.status == "pending":
                    self.addch(posy, posx,
                        ord("p"), COLOR_PAIR["order_pending"])
                else:
                    self.addch(posy, posx,
                        ord("o"), COLOR_PAIR["book_own"])

        if self.is_in_range(book.bid):
            posy = self.price_to_screen(book.bid)
            # pylint: disable=E1101
            self.addch(posy, posx,
                curses.ACS_HLINE, COLOR_PAIR["chart_up"])

        if self.is_in_range(book.ask):
            posy = self.price_to_screen(book.ask)
            # pylint: disable=E1101
            self.addch(posy, posx,
                curses.ACS_HLINE, COLOR_PAIR["chart_down"])


    def slot_history_changed(self, _sender, _data):
        """Slot for history changed"""
        self.change_type = TYPE_HISTORY
        self.do_paint()
        self.change_type = None

    def slot_orderbook_changed(self, _sender, _data):
        """Slot for orderbook changed"""
        self.change_type = TYPE_ORDERBOOK
        self.do_paint()
        self.change_type = None
